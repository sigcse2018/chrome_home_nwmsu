const tday = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat']
const tmonth = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']

/**
 * Get the current system date and time and update the associated display element.
 */
function GetClock() {

  // get the current date time from the system and load it into a variable d
  const d = new Date()

  // use the date functions to get each part
  const nday = d.getDay()
  const nmonth = d.getMonth()
  const ndate = d.getDate()
  const nyear = d.getFullYear()
  let nhour = d.getHours()
  let nmin = d.getMinutes()

  let ampm // variable that will be assigned either AM or PM

  if (nhour === 0) { ampm = ' AM'; nhour = 12 }
  else if (nhour < 12) { ampm = ' AM' }
  else if (nhour === 12) { ampm = ' PM' }
  else if (nhour > 12) { ampm = ' PM'; nhour -= 12 }  // subtract 12 for after noon hours

  if (nmin < 10) nmin = '0' + nmin  // add a leading zero to minutes when less than 10

  // find the html element with an id = "clockbox"
  // and set the inner HTML content to a nice date display
  document.getElementById('clockbox').innerHTML = '' + tday[nday] +
    ' ' + tmonth[nmonth] + ' ' + ndate + '  ' + nhour + ':' + nmin + ampm + ''
}
/**
 * Calculate a greeting and update the associated display element.
 */
function GetGreeting() {
  const d = new Date()
  const nhour = d.getHours()
  var t = 'Good evening'
  if (nhour < 12) { t = 'Good morning' }
  else if (nhour < 17) { t = 'Good afternoon' }
  document.getElementById('greeting').innerHTML = t + ', Bearcat!'
}

/** 
 * Set the window onload event handler to be a function 
 * that gets the clock and the greeting. Use built-in 
 * window.setInterval to call each function every 1000 milliseconds (1 sec).  
 */
window.onload = function () {
  GetClock()
  setInterval(GetClock, 1000)
  GetGreeting()
  setInterval(GetGreeting, 1000)
}
